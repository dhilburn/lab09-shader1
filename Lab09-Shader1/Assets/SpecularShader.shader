﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

Shader "Custom/Specular Shader" 
{
	Properties {
		_Color ("Color", Color) = (0,0,0,0)
		_SpecColour ("Specular Color", Color) = (1, 1, 1, 1)
		_Shininess("Shininess", float) = 10
	}
	SubShader 
	{		
		Pass
		{
			CGPROGRAM
			#pragma vertex vertexFunction
			#pragma fragment fragmentFunction
			#include "UnityCG.cginc"

			uniform float4 _Color;
			uniform float4 _SpecColour;
			uniform float _Shininess;

			uniform float4 _LightColor0;

			struct inputStruct
			{
				float4 vertexPos : POSITION;
				float3 vertexNormal : NORMAL;
			};

			struct outputStruct
			{
				float4 pixelPos : SV_POSITION;
				float4 pixelColor : COLOR;

				float3 normalDirection : TEXCOORD0;
				float4 pixelWorldPos : TEXCOORD1;
			};

			outputStruct vertexFunction(inputStruct input)
			{
				outputStruct toReturn;



				float4 inputNormal = float4(input.vertexNormal, 0.0);
				float4 cameraPos = float4(_WorldSpaceCameraPos.xyz, 1.0);



				float3 normalDirection = normalize(mul(inputNormal, unity_WorldToObject));
				float3 viewDirection = normalize(float3((cameraPos - mul(unity_ObjectToWorld, input.vertexPos)).xyz));

				toReturn.normalDirection = normalDirection;
				toReturn.pixelWorldPos = float4(viewDirection, 0.0);





				toReturn.pixelPos = mul(UNITY_MATRIX_MVP, input.vertexPos);

				return toReturn;
			}

			float4 fragmentFunction(outputStruct input) : COLOR
			{
				float attenuation = 1.0;

				float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);

				float3 normalLightAngle = dot(input.normalDirection, -lightDirection);

				float3 diffuseReflection = attenuation * _LightColor0.xyz * max(0.0, normalLightAngle);

				float3 specularReflection = reflect(lightDirection, input.normalDirection);
				specularReflection = dot(specularReflection, input.pixelWorldPos);
				specularReflection = max(0.0, specularReflection);
				specularReflection = pow(max(0.0, specularReflection), _Shininess);
				specularReflection = max(0.0, normalLightAngle) * specularReflection;

				float3 finalLight = specularReflection + diffuseReflection + UNITY_LIGHTMODEL_AMBIENT;

				input.pixelColor = float4(finalLight * _Color, 1.0);

				return input.pixelColor;
			}
			ENDCG
		}
	}
	//FallBack "Diffuse"
}
