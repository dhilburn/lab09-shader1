﻿// @author Darrick Hilburn
// This shader applies a material by applying a colored texture
//    that is affected by lambert lighting

Shader "Custom/Diffuse Shader" 
{
	Properties 
	{
		_Color("Color", Color) = (1, 1, 1, 1)
		_MainTex("Diffuse Texture", 2D) = "white"{}
	}
	SubShader 
	{
		Pass
		{
			Tags { "LightMode" = "ForwardBase" }

			CGPROGRAM
			#pragma vertex vertexFunction
			#pragma fragment fragmentFunction
			#include "UnityCG.cginc"

			// User-defined vars

			// Color Tint
			uniform half4 _Color;
			// Texture structure
			uniform sampler2D _MainTex;
			// Texture tiling & offset
			uniform float4 _MainTex_ST;

			// Unity defined variable for the lighting
			uniform float4 _LightColor0;

			// Input structure
			struct inputStruct
			{
				// Position of a vertex
				float4 vertexPos : POSITION;
				// UV Map of the model
				float4 textureCoord : TEXCOORD0;
				// Vertex normal
				float3 norm : NORMAL;
			};

			// Output structure
			struct outputStruct
			{
				// Position of a pixel
				float4 pixelPos : SV_POSITION;
				// Texture on the pixel
				float4 tex : TEXCOORD0;
				// Color to apply per fragment
				half4 col : COLOR;
			};

			// Vertex program
			outputStruct vertexFunction(inputStruct input)
			{
				outputStruct toReturn;
				// Calculate object and lighting normals
				float4 inputNormal = float4(input.norm, 0.0);
				float3 normalDirection = normalize(mul(inputNormal, unity_WorldToObject));
				float3 lightDirection = normalize(_WorldSpaceLightPos0);
				// Calculate diffuse reflection based on angle of light to object, light color, and color
				float3 normalLightAngle = dot(normalDirection, lightDirection);
				float3 diffuseReflection = _LightColor0.rgb * _Color.rgb * max(0.0, normalLightAngle);

				// Set color to diffuse lighting, calculate position, and set texture
				toReturn.col = float4(diffuseReflection, 1.0);
				toReturn.pixelPos = mul(UNITY_MATRIX_MVP, input.vertexPos);
				toReturn.tex = input.textureCoord;
				return toReturn;
			}

			// Fragment program
			half4 fragmentFunction(outputStruct input) : COLOR
			{
				// Apply the texture, distorting it based on the texture property and its tiling and offset
				float4 applyTexture = tex2D(_MainTex, _MainTex_ST.zw * input.tex.xy + _MainTex_ST.xy);
				//return _Color * applyTexture;
				// Output a color based on the passed structure's color and the above calculated texture
				return input.col * applyTexture;
			}
			ENDCG
		}
	}
	//FallBack "Diffuse"
}